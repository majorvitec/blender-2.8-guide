# Blender 2.8 Guide [2020]

Learning Blender 2.8 by creating models from the Guide

## Guide Theme Overview

### Simple Modeling
[Simple modeling Models](https://gitlab.com/majorvitec/blender-2.8-guide/-/tree/master/01_simple_modeling_exercises)

![Desk](/images/readme/desk.png "Desk")

### Basic Modeling
[Basic modeling Models](https://gitlab.com/majorvitec/blender-2.8-guide/-/tree/master/02_basic_modeling_exercises)
![Headphones](/images/readme/headphones.png "Headphones")

### Modifiers
[Modifiers Models](https://gitlab.com/majorvitec/blender-2.8-guide/-/tree/master/04_modifiers_exercices)
![Sofa](/images/readme/sofa.png "Sofa")

### Curves Modeling
[Curves modeling models](https://gitlab.com/majorvitec/blender-2.8-guide/-/tree/master/05_curves_modeling_exercises)

![3D Logo](/images/readme/3d_logo.png "3D Logo")

### Shaders and Node
[Shaders and Node Models](https://gitlab.com/majorvitec/blender-2.8-guide/-/tree/master/07_shaders_and_node_system)

![Robot](/images/readme/robo_front_more_light.png "Robot")

### Lighting and Shooting
[Lighting and Shooting Models](https://gitlab.com/majorvitec/blender-2.8-guide/-/tree/master/08_lighting_and_shooting)

![Studio](/images/readme/studio.png "Studio")

